occupied = {}
support = {}

def diplomacy_read(s):
    """
	read a single line and return list consisting
	of [army name, location, action, d] where
	d = name of city if action == MOVE or
	d = other army if action == SUPPORT or
	no d element if action == HOLD
	"""
    a = s.split()
    support[a[0]] = 0
    return a
	
def diplomacy_eval(armies):
    """ 
	recieve list of army info, updates locations and supports
    executes attacks and updates status if needed
    returns dictionary with key: army; value: status (location or [dead])
    """
    status = {}
    assert armies != []
    for i in armies:
        army = i[0]
        location = i[1]
        occupied[army] = location
        status[army] = location
        
        if len(i) > 2:
            act = i[2]
            if act == 'Move':
                occupied[army] = i[3]
                status[army] = i[3]
            if act == 'Support':
                support[i[3]] += 1
                
    # check for attacks and take away support from fighting armies
    fighting = []
    # this loop checks for fighting armies and invalidates support if needed
    for i in armies[:(len(armies)-1)]:
        army_i = i[0]
        #print(armies.index(i),i,army_i)
        for j in armies[armies.index(i)+1:]:
            army_j = j[0]
            if occupied[army_i] == occupied[army_j]:
                fighting.append([army_i,army_j])
                if len(i) > 2:
                   if i[2] == 'Support':
                        support[i[3]] -= 1
                if len(j) > 2:
                    if j[2] == 'Support':
                        support[j[3]] -= 1
    
    # now compare support for attacks and update status if [dead]
    for k in fighting:
        if support[k[0]] == support[k[1]]:
            status[k[0]] = '[dead]'
            status[k[1]] = '[dead]'
        elif support[k[0]] > support[k[1]]:
            status[k[1]] = '[dead]'
        else:
            status[k[0]] = '[dead]'
    
    assert status != {}
    return status
  
def diplomacy_print(w, status):
    """
    prints string
    w a writer
    army the name of each army
    status either location or [dead]
    """
    for key in status:
        w.write(key + " " + status[key] + "\n")
    
def diplomacy_solve(r,w):
    """
    r a reader
    w a writer
    """
    armies = []
    for s in r:
        a = diplomacy_read(s)
        armies.append(a)
    status = diplomacy_eval(armies)
    diplomacy_print(w, status)